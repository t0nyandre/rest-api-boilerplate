const ROOT_URL = "https://rest-boilerplate.localhost/api"

export const loginUser = async (dispatch, loginPayload) => {
    try {
        dispatch({type: "REQUEST_LOGIN"})
        let response = await fetch(`${ROOT_URL}/v1/auth/login`, { 
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(loginPayload)
        }
)
        let data = await response.json()
        const ssData = {
            user: {
                nickname: data.user.nickname,
                email: data.user.email,
            },
            token: data.token,
            token_exp: data.token_expires
        }

        if (data.user) {
            dispatch({ type: "LOGIN_SUCCESS", payload: ssData })
            localStorage.setItem("ss", JSON.stringify(ssData))
            return ssData
        }

        dispatch({ type: "LOGIN_ERROR", error: data.errors[0] })
        return;
    } catch(error) {
        dispatch({ type: "LOGIN_ERROR", error: error })
    }
}

export const logout = async (dispatch) => {
    try {
        dispatch({ type: "LOGOUT" })
        let response = await fetch(`${ROOT_URL}/v1/auth/logout`, {
            method: "POST",
            headers: { "Content-Type": "application/json" }
        })

        if (response.status == 204) {
            localStorage.removeItem("ss")
        } 

    } catch (error) {
        console.log(error)
    }
}