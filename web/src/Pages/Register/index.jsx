import React from "react"
import { Link } from "react-router-dom"

const handleSubmit = (e) => {
    e.preventDefault()
}

const Login = (props) => {
    return (
        <div className="min-h-screen bg-gray-50 flex flex-col justify-center">
            <div className="max-w-md w-full mx-auto">
                <div className="text-center font-medium text-3xl font-bold">
                    Sign in to your account
                </div>
                <div className="text-l text-center">
                    or <Link to="/register" className="font-medium text-l text-blue-500">sign up for an account</Link>
                </div>
            </div>
            <div className="max-w-md text-left w-full mx-auto mt-4 bg-white p-8 border rounded border-gray-300">
                <form onClick={handleSubmit} className="space-y-5">
                    <div>
                        <label htmlFor="email" className="text-sm font-bold text-gray-600 block">Email</label>
                        <input type="text" name="email" id="email" className="w-full p-2 border border-gray-300 rounded mt-1"/>
                    </div>
                    <div>
                        <label htmlFor="password" className="text-sm font-bold text-gray-600 block">Password</label>
                        <input type="password" name="password" id="password" className="w-full p-2 border border-gray-300 rounded mt-1"/>
                    </div>
                    <div>
                        <button className="w-full py-2 px-4 bg-blue-600 hover:bg-blue-700 rounded text-white text-sm">
                            Sign in
                        </button>
                    </div>
                    <div className="flex items-center justify-between">
                        <div></div>
                        <div><Link to="/forgot-password" className="font-medium text-sm text-blue-500">Forgot password?</Link></div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Login