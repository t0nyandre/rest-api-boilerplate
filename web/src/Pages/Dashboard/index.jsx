import React from "react"
import { useAuthDispatch, useAuthState } from "../../Context"
import { logout } from "../../Context/actions"

const Dashboard = (props) => {
    const dispatch = useAuthDispatch()
    const data = useAuthState()

    const handleLogout = () => {
        logout(dispatch)
        props.history.push("/login")
    }

    return (
        <div className="min-h-screen bg-gray-50 flex flex-col justify-center">
            <div className="max-w-md w-full mx-auto">
                <div className="text-center font-medium text-3xl font-bold">
                    <p>
                        Welcome, {data.user.nickname}!
                    </p>
                    <button
                        type="button"
                        onClick={handleLogout}
                        className="border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline">
                        Sign out
                    </button>
                </div>
            </div>
        </div>
    )
}

export default Dashboard