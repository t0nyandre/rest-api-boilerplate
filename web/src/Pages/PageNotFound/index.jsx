import React from "react"

const PageNotFound = (props) => {
    return (
        <div className="min-h-screen bg-gray-50 flex flex-col justify-center">
            <div className="max-w-md w-full mx-auto">
                <div className="text-center font-medium text-4xl font-bold">
                    404 Page Not Found!
                </div>
            </div>
        </div>
    )
}

export default PageNotFound