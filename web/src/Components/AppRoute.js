import { Redirect, Route } from "react-router-dom";
import { useAuthState } from "../Context"

export const AppRoute = ({component: Component, path, meta, ...rest}) => {
    const data = useAuthState()
    
    return (
        <Route
            path={path}
            render={props =>
                meta.isPrivate && !Boolean(data.token) ? (
                    <Redirect to={{pathname: "/login"}} />
                ) : (
                    meta.isGuest && Boolean(data.token) ? (
                        <Redirect to={{pathname: "/dashboard"}} />
                    ) : (
                        <Component {...props} />
                    )
                )
            }
            {...rest}
        />
    )
}