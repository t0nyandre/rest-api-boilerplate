import Dashboard from "../Pages/Dashboard"
import Login from "../Pages/Login"
import PageNotFound from "../Pages/PageNotFound"

const routes = [
    {
        path: "/login",
        component: Login,
        meta: {
            isPrivate: false,
            isGuest: true,
        }
    },
    {
        path: "/dashboard",
        component: Dashboard,
        meta: {
            isPrivate: true,
            isGuest: false,
        }
    },
    {
        path: "/*",
        component: PageNotFound,
        meta: {
            isPrivate: false,
            isGuest: false,
        }
    }
]

export default routes