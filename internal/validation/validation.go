package validation

import (
	"fmt"
	"reflect"
	"strings"

	"gopkg.in/go-playground/validator.v9"
)

// ValidateStruct take one input, the struct that needs validation and returns a boolean and errors, if any
func ValidateStruct(data interface{}) (bool, map[string]string) {
	validate := validator.New()

	if err := validate.Struct(data); err != nil {
		if err, ok := err.(*validator.InvalidValidationError); ok {
			panic(err)
		}

		errors := make(map[string]string)
		reflected := reflect.ValueOf(data)

		for _, err := range err.(validator.ValidationErrors) {
			var name string

			field, _ := reflected.Type().FieldByName(err.StructField())

			if n := field.Tag.Get("json"); name == "" {
				n = strings.ToLower(err.StructField())
				name = strings.Split(n, ",")[0]
			}

			switch err.Tag() {
			case "required":
				errors[name] = fmt.Sprintf("The %s is required!", name)
				break
			case "min":
				errors[name] = fmt.Sprintf("The %s is not strong enough!", name)
				break
			case "email":
				errors[name] = "You need to provide a valid email!"
				break
			default:
				errors[name] = fmt.Sprintf("The %s is invalid", name)
				break
			}
		}

		return false, errors
	}
	return true, nil
}
