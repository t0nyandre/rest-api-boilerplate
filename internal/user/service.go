package user

import (
	"context"
	"fmt"

	"github.com/lithammer/shortuuid"
	"go.uber.org/zap"
)

// Service ...
type Service interface {
	Create(ctx context.Context, input *User) (*User, error)
	Get(ctx context.Context, input *User) (*User, error)
	UserValid(ctx context.Context, input *User) (bool, error)
}

type service struct {
	repo   Repository
	logger *zap.SugaredLogger
}

// NewService ...
func NewService(repo Repository, logger *zap.SugaredLogger) Service {
	return service{repo, logger}
}

func (s service) Create(ctx context.Context, input *User) (*User, error) {
	input.ID = shortuuid.New()
	user, err := s.repo.Create(input)
	if err != nil {
		s.logger.Errorw(
			"Error creating user",
			"email", input.Email,
			"id", input.ID,
			"error", err.Error(),
		)
		return nil, err
	}
	return user, nil
}

func (s service) UserValid(ctx context.Context, user *User) (bool, error) {
	user, err := s.Get(ctx, user)
	if err != nil {
		return false, err
	}
	if !user.Confirmed {
		return false, fmt.Errorf("account is not confirmed")
	}
	return true, nil
}

func (s service) Get(ctx context.Context, input *User) (*User, error) {
	user := &User{
		Nickname: input.Nickname,
		Email:    input.Email,
	}
	user, err := s.repo.Get(user)
	if err != nil {
		s.logger.Errorw(
			"Could not get user",
			"input", input,
			"error", err.Error(),
		)
		return nil, err
	}
	return user, nil
}
