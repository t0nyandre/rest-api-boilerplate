package user

import (
	"context"
	"time"

	"github.com/alexedwards/argon2id"
	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/logger"
)

// User database model
type User struct {
	ID        string     `json:"_id,omitempty"`
	Nickname  string     `json:"nickname,omitempty"`
	Password  string     `json:"-"`
	Email     string     `json:"email,omitempty"`
	Confirmed bool       `json:"confirmed,omitempty"`
	CreatedAt time.Time  `json:"created_at,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
}

func (u *User) hashPassword() string {
	hash, err := argon2id.CreateHash(u.Password, &argon2id.Params{
		Iterations:  3,
		Memory:      4096,
		Parallelism: 2,
		SaltLength:  16,
		KeyLength:   32,
	})
	if err != nil {
		logger := logger.NewLogger()
		logger.Panicw(
			"Could not hash password",
			"email", u.Email,
			"error", err.Error(),
		)
	}

	return hash
}

// VerifyPassword returns true if password provided matches the password in database. If it doesn't match it will return false
func (u *User) VerifyPassword(password string) bool {
	match, err := argon2id.ComparePasswordAndHash(password, u.Password)
	if err != nil {
		return false
	}

	return match
}

// BeforeInsert will run before the data is saved in the database
func (u *User) BeforeInsert(ctx context.Context) (context.Context, error) {
	if u.CreatedAt.IsZero() {
		u.CreatedAt = time.Now()
		u.UpdatedAt = nil
	}
	u.Password = u.hashPassword()
	return ctx, nil
}

// BeforeUpdate will run before the updated data is saved in the database
func (u *User) BeforeUpdate(ctx context.Context) (context.Context, error) {
	t := time.Now()
	u.UpdatedAt = &t
	return ctx, nil
}
