package user

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/response"
	"go.uber.org/zap"
)

// RegisterHandlers builds a router for User
func RegisterHandlers(service Service, logger *zap.SugaredLogger) http.Handler {
	res := resource{service, logger}
	r := chi.NewRouter()
	r.Get("/{nickname}", res.get)
	return r
}

type resource struct {
	service Service
	logger  *zap.SugaredLogger
}

func (r resource) get(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	user := &User{Nickname: chi.URLParam(req, "nickname")}
	user, err := r.service.Get(ctx, user)
	if err != nil {
		response.NewErrResponse(w, http.StatusNotFound, fmt.Sprintf("could not find user: %s", chi.URLParam(req, "nickname")))
		return
	}

	response.NewJSONResponse(w, http.StatusOK, &user)
}
