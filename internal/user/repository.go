package user

import "github.com/go-pg/pg/v10"

// Repository ...
type Repository interface {
	Create(user *User) (*User, error)
	Get(user *User) (*User, error)
}

// Repository holds db connection
type repository struct {
	db *pg.DB
}

// NewRepository ...
func NewRepository(db *pg.DB) Repository {
	return repository{db}
}

// Create saves a new User record in the database with a hashed password
// It returns the saved User
func (r repository) Create(user *User) (*User, error) {
	user.Confirmed = true
	_, err := r.db.Model(user).Returning("*").Insert()
	if err != nil {
		return nil, err
	}
	return user, err
}

// Get returns the User with the specified handle
func (r repository) Get(user *User) (*User, error) {
	err := r.db.Model(user).Where("nickname = ? OR email = ?", user.Nickname, user.Email).Select()
	if err != nil {
		return nil, err
	}
	return user, nil
}
