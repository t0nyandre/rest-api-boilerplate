package logger

import (
	"fmt"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// NewLogger creates a new instance of the zap sugared logger
func NewLogger() *zap.SugaredLogger {
	dateString := time.Now().Format("20060102")
	cfg := zap.Config{
		Level:            zap.NewAtomicLevelAt(zap.DebugLevel),
		Encoding:         "json",
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:  "msg",
			LevelKey:    "level",
			TimeKey:     "timestamp",
			EncodeTime:  zapcore.RFC3339NanoTimeEncoder,
			EncodeLevel: zapcore.LowercaseLevelEncoder,
		},
		OutputPaths: []string{"stdout", fmt.Sprintf("./logs/%s.txt", dateString)},
	}

	l, err := cfg.Build()
	if err != nil {
		panic(err)
	}
	logger := l.Sugar()

	return logger
}
