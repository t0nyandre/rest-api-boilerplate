package postgres

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/go-pg/pg/v10"
	"go.uber.org/zap"
)

// NewPostgresConnect will return a new connection to the postgres database
func NewPostgresConnect(ctx context.Context, logger *zap.SugaredLogger) *pg.DB {
	conn := pg.Connect(&pg.Options{
		Addr:            fmt.Sprintf("%s:%s", os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_PORT")),
		User:            os.Getenv("POSTGRES_USER"),
		Password:        os.Getenv("POSTGRES_PASSWORD"),
		Database:        os.Getenv("POSTGRES_DATABASE"),
		ApplicationName: os.Getenv("APP_NAME"),
	})

	if err := conn.Ping(ctx); err != nil {
		logger.Warnw(
			"Retrying database connection in 5 seconds",
			"appName", os.Getenv("APP_NAME"),
			"user", os.Getenv("POSTGRES_USER"),
			"database", os.Getenv("POSTGRES_DATABASE"),
			"error", err.Error(),
		)
		time.Sleep(time.Duration(5) * time.Second)
		return NewPostgresConnect(ctx, logger)
	}

	logger.Infow(
		"Successfully connected to database",
		"database", os.Getenv("POSTGRES_DATABASE"),
	)

	return conn
}
