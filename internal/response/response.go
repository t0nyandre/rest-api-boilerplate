package response

import (
	"encoding/json"
	"net/http"
)

type response struct {
	Payload    interface{}
	StatusCode int
}

// NewJSONResponse will return a json response
func NewJSONResponse(w http.ResponseWriter, statusCode int, payload interface{}) {
	resDto := response{payload, statusCode}
	res, err := json.Marshal(resDto.Payload)
	if err != nil {
		NewErrResponse(w, http.StatusInternalServerError, map[string]string{"json": "could not marshal response payload"})
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(resDto.StatusCode)
	w.Write([]byte(res))
}

// NewErrResponse structures the error messages with a statuscode
func NewErrResponse(w http.ResponseWriter, statusCode int, data interface{}) {
	NewJSONResponse(w, statusCode, map[string]interface{}{"status": statusCode, "errors": data})
}
