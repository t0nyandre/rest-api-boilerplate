package auth

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/mssola/user_agent"
	jsonwt "gitlab.com/t0nyandre/rest-api-boilerplate/internal/jwt"
	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/response"
	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/user"
	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/validation"
	"go.uber.org/zap"
)

func RegisterHandlers(user user.Service, auth Service, logger *zap.SugaredLogger) http.Handler {
	res := resource{user, auth, logger}
	r := chi.NewRouter()
	r.Post("/register", res.register)
	r.Post("/login", res.login)
	r.Post("/logout", res.logout)
	r.Post("/refresh", res.refresh)

	r.Group(func(r chi.Router) {
		r.Use(Authenticated)
		r.Get("/me", res.me)
	})
	return r
}

type resource struct {
	user   user.Service
	auth   Service
	logger *zap.SugaredLogger
}

type registerInput struct {
	Nickname string `json:"nickname,omitempty" validate:"required"`
	Email    string `json:"email,omitempty" validate:"required,email"`
	Password string `json:"password,omitempty" validate:"required,min=6"`
}

func (r resource) register(w http.ResponseWriter, req *http.Request) {
	input := registerInput{}
	ctx := req.Context()
	json.NewDecoder(req.Body).Decode(&input)

	if ok, errors := validation.ValidateStruct(input); !ok {
		response.NewErrResponse(w, http.StatusUnprocessableEntity, errors)
		return
	}

	user := &user.User{
		Nickname: input.Nickname,
		Email:    input.Email,
		Password: input.Password,
	}

	user, err := r.user.Create(ctx, user)
	if err != nil {
		response.NewErrResponse(w, http.StatusBadRequest, map[string]string{"register": fmt.Sprintf("could not create user: %s", input.Nickname)})
		return
	}

	response.NewJSONResponse(w, http.StatusCreated, user)
}

func (r resource) logout(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	sessionID, err := req.Cookie("ssid")
	if err != nil {
		response.NewErrResponse(w, http.StatusBadRequest, map[string]string{"logout": "no current session to log out"})
		return
	}

	auth, err := r.auth.GetAuth(ctx, "", sessionID.Value)
	auth.Message = "user logged out"

	if ok, err := r.auth.RevokeSession(ctx, auth); !ok {
		response.NewErrResponse(w, http.StatusInternalServerError, map[string]string{"logout": fmt.Sprintf("something went wrong: %s", err.Error())})
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:     "rfid",
		Path:     "/api/v1/auth/refresh",
		HttpOnly: true,
		MaxAge:   -1,
		Secure:   true,
	})

	http.SetCookie(w, &http.Cookie{
		Name:     "ssid",
		Path:     "/",
		HttpOnly: true,
		MaxAge:   -1,
		Secure:   true,
	})

	response.NewJSONResponse(w, http.StatusNoContent, nil)
}

type loginInput struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

func (r resource) login(w http.ResponseWriter, req *http.Request) {
	input := loginInput{}
	ctx := req.Context()
	json.NewDecoder(req.Body).Decode(&input)

	user, err := r.user.Get(ctx, &user.User{Email: input.Email})
	if err != nil {
		response.NewErrResponse(w, http.StatusBadRequest, map[string]string{"login": "wrong username and/or password"})
		return
	}

	valid := user.VerifyPassword(input.Password)
	if !valid {
		response.NewErrResponse(w, http.StatusBadRequest, map[string]string{"login": "wrong username and/or password"})
		return
	}

	if ok, err := r.user.UserValid(ctx, user); !ok {
		response.NewErrResponse(w, http.StatusBadRequest, map[string]string{"login": err.Error()})
		return
	}

	ua := user_agent.New(req.UserAgent())
	browserName, _ := ua.Browser()

	auth := &Auth{
		User:   user,
		Device: browserName,
		OS:     ua.OSInfo().Name,
		IP:     req.RemoteAddr,
	}

	tokens, err := r.auth.PresistAuth(ctx, auth)
	if err != nil {
		response.NewErrResponse(w, http.StatusBadRequest, map[string]string{"login": err.Error()})
		return
	}

	accessToken, err := jsonwt.GenerateToken(*tokens.User)
	if err != nil {
		response.NewErrResponse(w, http.StatusBadRequest, map[string]string{"login": err.Error()})
		return
	}
	tokens.AccessToken = accessToken

	exp, _ := time.ParseDuration(fmt.Sprintf("%vm", os.Getenv("TOKEN_EXPIRE")))
	tokens.AccessExpiresAt = time.Now().Add(exp)

	http.SetCookie(w, &http.Cookie{
		Name:     "rfid",
		Value:    tokens.RefreshToken,
		Path:     "/api/v1/auth/refresh",
		HttpOnly: true,
		MaxAge:   int(tokens.RefreshExpiresAt.Unix()),
		Secure:   true,
	})

	http.SetCookie(w, &http.Cookie{
		Name:     "ssid",
		Value:    tokens.SessionID,
		Path:     "/",
		HttpOnly: true,
		MaxAge:   int(tokens.RefreshExpiresAt.Unix()),
		Secure:   true,
	})

	r.logger.Infow(
		"User logged in",
		"user", user.Nickname,
	)

	response.NewJSONResponse(w, http.StatusOK, tokens)
}

func (r resource) me(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	userID := ctx.Value(jsonwt.UserIDKey)
	userNickname := ctx.Value(jsonwt.UserNicknameKey)

	user := &user.User{
		ID:       userID.(string),
		Nickname: userNickname.(string),
	}

	user, err := r.user.Get(ctx, user)
	if err != nil {
		response.NewErrResponse(w, http.StatusInternalServerError, map[string]string{"me": "something went wrong"})
		return
	}

	response.NewJSONResponse(w, http.StatusOK, user)
}

func (r resource) refresh(w http.ResponseWriter, req *http.Request) {
	refresh, err := req.Cookie("rfid")
	if err != nil {
		response.NewErrResponse(w, http.StatusBadRequest, map[string]string{"refresh": "invalid or no refresh token"})
		return
	}
	auth, err := r.auth.GetAuth(req.Context(), refresh.Value, "")
	if err != nil {
		response.NewErrResponse(w, http.StatusBadRequest, map[string]string{"refresh": fmt.Sprintf("refresh token expired or revoked: %s", err.Error())})
		return
	}

	if ok, err := r.user.UserValid(req.Context(), auth.User); !ok {
		response.NewErrResponse(w, http.StatusBadRequest, map[string]string{"login": err.Error()})
		return
	}

	if auth.ExpiresAt.Before(time.Now()) {
		response.NewErrResponse(w, http.StatusBadRequest, map[string]string{"refresh": "refresh token expired"})
		return
	}

	accessToken, err := jsonwt.GenerateToken(user.User{ID: auth.User.ID, Nickname: auth.User.Nickname})
	if err != nil {
		response.NewErrResponse(w, http.StatusInternalServerError, map[string]string{"refresh": fmt.Sprintf("something went wrong: %s", err.Error())})
		return
	}

	exp, _ := time.ParseDuration(fmt.Sprintf("%vm", os.Getenv("TOKEN_EXPIRE")))
	accessTokenExpires := time.Now().Add(exp)

	if auth.ExpiresAt.Unix() < time.Now().AddDate(0, 0, 3).Unix() {
		eat, err := strconv.Atoi(os.Getenv("REFRESH_EXPIRE"))
		if err != nil {
			response.NewErrResponse(w, http.StatusInternalServerError, map[string]string{"refresh": fmt.Sprintf("something went wrong: %s", err.Error())})
			return
		}
		auth.ExpiresAt = time.Now().AddDate(0, 0, eat)
		if ok, err := r.auth.ExtendToken(req.Context(), auth); !ok {
			response.NewErrResponse(w, http.StatusInternalServerError, map[string]string{"refresh": fmt.Sprintf("something went wrong: %s", err.Error())})
			return
		}

		http.SetCookie(w, &http.Cookie{
			Name:     "rfid",
			Value:    auth.RefreshToken,
			Path:     "/api/v1/auth/refresh",
			HttpOnly: true,
			MaxAge:   int(auth.ExpiresAt.Unix()),
			Secure:   true,
		})

		http.SetCookie(w, &http.Cookie{
			Name:     "ssid",
			Value:    auth.ID,
			Path:     "/",
			HttpOnly: true,
			MaxAge:   int(auth.ExpiresAt.Unix()),
			Secure:   true,
		})

		r.logger.Infow(
			"refresh-token successfully refreshed",
			"userID", auth.User.ID,
		)
	}

	r.logger.Infow(
		"access-token successfully refreshed",
		"userID", auth.User.ID,
	)

	response.NewJSONResponse(w, http.StatusOK, &Token{
		User:             auth.User,
		AccessToken:      accessToken,
		AccessExpiresAt:  accessTokenExpires,
		RefreshToken:     auth.RefreshToken,
		RefreshExpiresAt: auth.ExpiresAt,
	})
	return
}
