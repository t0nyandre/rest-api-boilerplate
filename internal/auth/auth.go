package auth

import (
	"context"
	"time"

	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/user"
)

// Auth database model
type Auth struct {
	ID string `json:"_id"`

	User   *user.User `json:"user" pg:"rel:has-one"`
	UserID string     `json:"-"`

	Device string `json:"device"`
	OS     string `json:"os"`
	IP     string `json:"ip"`

	RefreshToken string `json:"-"`
	Message      string `json:"message"`

	ExpiresAt time.Time  `json:"expires_at"`
	CreatedAt time.Time  `json:"created_at"`
	RevokedAt *time.Time `json:"revoked_at"`
}

// Token returns user, refresh token and access token to the user when logged in
type Token struct {
	User             *user.User `json:"user,omitempty"`
	SessionID        string     `json:"-"`
	AccessToken      string     `json:"token,omitempty"`
	AccessExpiresAt  time.Time  `json:"token_expires,omitempty"`
	RefreshToken     string     `json:"-"`
	RefreshExpiresAt time.Time  `json:"-"`
}

// BeforeInsert creates the timestamp for the auth content
func (a *Auth) BeforeInsert(ctx context.Context) (context.Context, error) {
	if a.CreatedAt.IsZero() {
		a.CreatedAt = time.Now()
	}
	return ctx, nil
}
