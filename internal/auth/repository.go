package auth

import (
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/lithammer/shortuuid"
)

// Repository ...
type Repository interface {
	Create(auth *Auth) (*Token, error)
	Get(auth *Auth) (*Auth, error)
	Update(auth *Auth) (*Auth, error)
	Revoke(auth *Auth) (bool, error)
}

// Holds db connection
type repository struct {
	db *pg.DB
}

// NewRepository ...
func NewRepository(db *pg.DB) Repository {
	return repository{db}
}

func (r repository) Create(auth *Auth) (*Token, error) {
	auth.ID = shortuuid.New()
	auth.UserID = auth.User.ID
	_, err := r.db.Model(auth).Returning("*").Insert()
	if err != nil {
		return nil, err
	}

	return &Token{
		User:             auth.User,
		SessionID:        auth.ID,
		RefreshToken:     auth.RefreshToken,
		RefreshExpiresAt: auth.ExpiresAt,
	}, nil
}

func (r repository) Get(auth *Auth) (*Auth, error) {
	err := r.db.Model(auth).Where("refresh_token = ? OR auth.id = ? AND revoked_at IS NULL", auth.RefreshToken, auth.ID).Relation("User").Select()
	if err != nil {
		return nil, err
	}
	return auth, nil
}

func (r repository) Update(auth *Auth) (*Auth, error) {
	_, err := r.db.Model(auth).Where("user_id = ? AND refresh_token = ? AND revoked_at IS NULL AND expires_at > NOW()", auth.User.ID, auth.RefreshToken).Returning("*").Update()
	if err != nil {
		return nil, err
	}
	return auth, nil
}

func (r repository) Revoke(auth *Auth) (bool, error) {
	t := time.Now()
	auth.RevokedAt = &t
	if _, err := r.db.Model(auth).Where("id = ?", auth.ID).Relation("User").Update(); err != nil {
		return false, err
	}
	return true, nil
}
