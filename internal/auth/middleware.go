package auth

import (
	"context"
	"net/http"

	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/jwt"
	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/response"
)

// Authenticated checks if the jwt token is valid if at all provided to view set content
func Authenticated(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		jwtToken, err := getJWT(req.Header.Get("Authorization"))
		if err != nil {
			response.NewErrResponse(w, http.StatusBadRequest, err.Error())
			return
		}
		payload, err := jwt.ValidateToken(jwtToken)
		if err != nil {
			if err.Error() == "token expired" {
				response.NewErrResponse(w, http.StatusUnauthorized, err.Error())
				return
			}
			response.NewErrResponse(w, http.StatusBadRequest, err.Error())
			return
		}
		ctx := context.WithValue(req.Context(), jwt.UserIDKey, payload.(jwt.TokenPayload).UserID)
		ctx = context.WithValue(ctx, jwt.UserNicknameKey, payload.(jwt.TokenPayload).UserNickname)
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}
