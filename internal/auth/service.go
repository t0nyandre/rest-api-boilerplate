package auth

import (
	"context"
	"os"
	"strconv"
	"time"

	"github.com/google/uuid"
	"go.uber.org/zap"
)

type Service interface {
	PresistAuth(ctx context.Context, auth *Auth) (*Token, error)
	GetAuth(ctx context.Context, refresh, sessionID string) (*Auth, error)
	ExtendToken(ctx context.Context, auth *Auth) (bool, error)
	RevokeSession(ctx context.Context, auth *Auth) (bool, error)
}

type service struct {
	repo   Repository
	logger *zap.SugaredLogger
}

func NewService(repo Repository, logger *zap.SugaredLogger) Service {
	return service{repo, logger}
}

func (s service) PresistAuth(ctx context.Context, auth *Auth) (*Token, error) {
	expiresAt, err := strconv.Atoi(os.Getenv("REFRESH_EXPIRE"))
	if err != nil {
		s.logger.Errorw(
			"Error converting REFRESH_EXPIRE env to int",
			"error", err.Error(),
		)
		return nil, err
	}
	auth.ExpiresAt = time.Now().AddDate(0, 0, expiresAt)

	refreshToken, err := uuid.NewRandom()
	if err != nil {
		s.logger.Errorw(
			"Error creating refresh token",
			"email", auth.User.Email,
			"error", err.Error(),
		)
		return nil, err
	}
	auth.RefreshToken = refreshToken.String()

	tokens, err := s.repo.Create(auth)
	if err != nil {
		s.logger.Errorw(
			"Error storing auth in database",
			"email", auth.User.Email,
			"handle", auth.User.Nickname,
			"error", err.Error(),
		)
		return nil, err
	}
	return tokens, nil
}

func (s service) GetAuth(ctx context.Context, refresh, sessionID string) (*Auth, error) {
	auth := &Auth{
		RefreshToken: refresh,
		ID:           sessionID,
	}
	auth, err := s.repo.Get(auth)
	if err != nil {
		return nil, err
	}
	return auth, nil
}

func (s service) RevokeSession(ctx context.Context, auth *Auth) (bool, error) {
	if ok, err := s.repo.Revoke(auth); !ok {
		return false, err
	}
	return true, nil
}

func (s service) ExtendToken(ctx context.Context, auth *Auth) (bool, error) {
	_, err := s.repo.Update(auth)
	if err != nil {
		return false, err
	}
	return true, nil
}
