package auth

import (
	"fmt"
	"strings"
)

func getJWT(authHeader string) (string, error) {
	tokenSlice := strings.Split(authHeader, "Bearer ")
	if len(tokenSlice) != 2 || tokenSlice[1] == "" {
		return "", fmt.Errorf("not allowed here")
	}

	return tokenSlice[1], nil
}
