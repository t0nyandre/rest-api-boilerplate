package jwt

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/user"
)

type key int

const (
	UserIDKey       = iota
	UserNicknameKey = iota
	SessionIDKey    = iota
)

// TokenClaims is the content the jwt will hold
type TokenClaims struct {
	UserID       string `json:"user_id"`
	UserNickname string `json:"nickname"`
	//TODO: Add roles
	// Role string `json:"user_role"`
	jwt.StandardClaims
}

// TokenPayload ...
type TokenPayload struct {
	UserID       string
	UserNickname string
}

// GenerateToken generates, signes and returns the JWT token
func GenerateToken(user user.User) (string, error) {
	exp, err := strconv.Atoi(os.Getenv("TOKEN_EXPIRE"))
	if err != nil {
		return "", err
	}
	claims := &TokenClaims{
		user.ID,
		user.Nickname,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Unix() + (60 * int64(exp)),
			IssuedAt:  time.Now().Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS384, claims)
	ss, err := token.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return "", err
	}
	return ss, nil
}

// ValidateToken will validate the token and return its content
func ValidateToken(tokenString string) (interface{}, error) {
	payload := TokenPayload{}
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("invalid signing method")
		}

		return []byte(os.Getenv("TOKEN_SECRET")), nil
	})

	if token.Valid {
		if claims, ok := token.Claims.(jwt.MapClaims); ok {
			payload.UserID = claims["user_id"].(string)
			payload.UserNickname = claims["nickname"].(string)
			return payload, nil
		}
	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			return nil, fmt.Errorf("you did not provide a valid bearer token")
		} else if ve.Errors&jwt.ValidationErrorExpired != 0 {
			return nil, fmt.Errorf("token expired")
		} else {
			return nil, fmt.Errorf("couldn't handle this token")
		}
	}

	return nil, fmt.Errorf("couldn't handle this token")
}
