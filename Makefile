#!make
include config/env/.env

MIGRATE := docker run -v $(shell pwd)/migrations:/migrations --network host --user $(shell id -u):$(shell id -g) migrate/migrate:v4.14.1 -path=/migrations/ -database postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${APP_HOST}:${POSTGRES_PORT}/${POSTGRES_DATABASE}
DEV := docker-compose -f docker/docker.development.yml

prune-containers:
	@echo "If you want to clean up all stopped containers you can do so now; "
	@docker container prune

build-dev:
	@$(DEV) build

dev: prune-containers build-dev
	@$(DEV) up

migrate:
	@echo "Running all new database migrations ..."
	@$(MIGRATE) up

migrate-new:
	@read -p "Enter the name of the new migration: " name; \
	$(MIGRATE) create -ext sql -dir /migrations/ $${name}

migrate-down:
	@echo "Reverting database to the last migrations step ..."
	@$(MIGRATE) down 1

migrate-reset:
	@echo "Resetting database ..."
	@$(MIGRATE) drop -f
	@echo "Running all database migrations ..."
	@$(MIGRATE) up

.PHONY: dev build-dev migrate migrate-new migrate-reset migrate-down prune-containers