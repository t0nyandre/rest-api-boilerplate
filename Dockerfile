FROM golang:1.15-alpine AS builder
ENV GO111MODULE=on
WORKDIR /src
COPY go.mod .
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o rest ./cmd/rest

FROM alpine:latest
WORKDIR /app
COPY --from=builder /src/rest .

EXPOSE 4000
ENTRYPOINT [ "/app/rest" ]