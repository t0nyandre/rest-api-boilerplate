package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-pg/pg/v10"
	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/auth"
	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/logger"
	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/postgres"
	"gitlab.com/t0nyandre/rest-api-boilerplate/internal/user"
	"go.uber.org/zap"
)

func main() {
	logger := logger.NewLogger()
	ctx := context.Background()

	db := postgres.NewPostgresConnect(ctx, logger)
	defer db.Close()

	server := &http.Server{
		Addr:         fmt.Sprintf("%s:%s", os.Getenv("APP_HOST"), os.Getenv("APP_PORT")),
		Handler:      buildRouter(logger, db),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	logger.Infow(
		fmt.Sprintf("Server is now listening to port %s ..", os.Getenv("APP_PORT")),
	)
	err := server.ListenAndServe()
	if err != nil {
		logger.Fatalw(
			"Error starting REST API",
			"host", os.Getenv("APP_HOST"),
			"port", os.Getenv("APP_PORT"),
			"error", err.Error(),
		)
	}
}

func buildRouter(logger *zap.SugaredLogger, db *pg.DB) *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.RealIP)
	r.Route("/v1", func(r chi.Router) {
		r.Mount("/users", user.RegisterHandlers(
			user.NewService(user.NewRepository(db), logger), logger,
		))
		r.Mount("/auth", auth.RegisterHandlers(
			user.NewService(user.NewRepository(db), logger),
			auth.NewService(auth.NewRepository(db), logger),
			logger,
		))
	})
	return r
}
