CREATE TABLE "auths" (
    "id" varchar(25),
    "user_id" varchar(25) NOT NULL,
    "device" varchar(100) NOT NULL,
    "os" varchar(100) NULL,
    "ip" varchar(100) NOT NULL,
    "refresh_token" text NOT NULL,
    "message" varchar(120) NULL,
    "expires_at" timestamp with time zone NOT NULL,
    "created_at" timestamp with time zone NOT NULL,
    "revoked_at" timestamp with time zone,
    PRIMARY KEY ("user_id", "refresh_token"),
    FOREIGN KEY ("user_id") REFERENCES users(id)
);