CREATE TABLE "users" (
    "id" varchar(25) PRIMARY KEY,
    "nickname" varchar(80) UNIQUE NOT NULL,
    "email" varchar(254) UNIQUE NOT NULL,
    "password" text NOT NULL,
    "confirmed" boolean DEFAULT false,
    "created_at" timestamp with time zone NOT NULL,
    "updated_at" timestamp with time zone
);